// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAWr2HrYyLarj6r1fzoN0hX-m7qRtUDyHE",
    authDomain: "controle-43bc7.firebaseapp.com",
    projectId: "controle-43bc7",
    storageBucket: "controle-43bc7.appspot.com",
    messagingSenderId: "349361098713",
    appId: "1:349361098713:web:c1b646e0332b6027e753c8",
    measurementId: "G-H50TRZK8KK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
