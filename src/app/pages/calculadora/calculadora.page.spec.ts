import { FormsModule } from '@angular/forms';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';
import { By } from '@angular/platform-browser';

describe('CalculadoraPage', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraPage ],
      imports: [ReactiveFormsModule, IonicModule.forRoot(), FormsModule],

    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement; 
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(model).toBeTruthy();
  });

  it('deve ter um botão de limpar os campos', () => {
    expect(model.limpar).toBeDefined();
  });

  it('deve ter um título', () => {
      expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o título', ()=>{
    const result = view.querySelector('.title').textContent;
    expect(result).toEqual('Calculadora')
  });

  it('deve estar com o botão desabilitado', () => {
    expect(model.form.invalid).toBeTrue();
  });
  
  // Divide
  it('divide com clique no botão', () => {
    // arrange
    model.form.controls.input1.setValue(20);
    model.form.controls.input2.setValue(5);

    // act
    const divide = fixture.debugElement.query(By.css('#divide'));
    divide.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('4');
  });

  // Multiplica
  it('multiplica com clique no botão', () => {
    // arrange
    model.form.controls.input1.setValue(5);
    model.form.controls.input2.setValue(5);

    // act
    const multiplica = fixture.debugElement.query(By.css('#multiplica'));
    multiplica.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('25');
  });

  // Subtrai
  it('subtrai com clique no botão', () => {
    // arrange
    model.form.controls.input1.setValue(10);
    model.form.controls.input2.setValue(5);

    // act
    const subtrai = fixture.debugElement.query(By.css('#subtrai'));
    subtrai.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('5');
  });

  // Soma
  it('soma com clique no botão', () => {
    // arrange
    model.form.controls.input1.setValue(10);
    model.form.controls.input2.setValue(5);

    // act
    const soma = fixture.debugElement.query(By.css('#soma'));
    soma.triggerEventHandler('click', null);
    fixture.detectChanges();

    // assert
    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('15');
  });
});
